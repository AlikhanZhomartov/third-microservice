package main

import (
	"context"
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"go-final-third/protobp/proto"
	"google.golang.org/grpc"
	"log"
	"net"
	"strconv"
)

type Server struct {
proto.UnimplementedEmployeeMainServiceServer
proto.UnimplementedCreateVacancyServiceServer
proto.UnimplementedOwnVacancyServiceServer
proto.UnimplementedDeleteVacancyServiceServer
proto.UnimplementedResponsesListServiceServer
proto.UnimplementedDeleteResponseServiceServer
}
type Vacancy struct {
	Id             uint32
	JobTitle       string
	CompanyTitle   string
	City           string
	Salary         int32
	JobDescription string
	UserId         uint32
}

type Responses struct {
	JobTitle string
	CompanyTitle string
	ResponseId int32
	ResumeId int32
}
func (s *Server) EmployeeMain(req *proto.EmployeeMainRequest, response proto.EmployeeMainService_EmployeeMainServer) error {

	db, err := sql.Open("mysql", "root:2131@tcp(127.0.0.1:3306)/final_second")
	if err != nil {
		panic(err)
	}

	defer db.Close()

	resSql, err := db.Query("select * from `vacancy` order by `Id` DESC ")
	if err != nil {
		panic(err)
	}


	if req.GetMessage() == "employee" {

		for resSql.Next() {
			var vacancy Vacancy
			resSql.Scan(&vacancy.Id, &vacancy.JobTitle, &vacancy.CompanyTitle, &vacancy.City, &vacancy.Salary, &vacancy.JobDescription, &vacancy.UserId)

			a := proto.EmployeeVacancies{Id: vacancy.Id, JobTitle: vacancy.JobTitle, CompanyTitle: vacancy.CompanyTitle, City: vacancy.City, Salary: vacancy.Salary, JobDescription: vacancy.JobDescription, EmployeeId: vacancy.UserId}
			res := &proto.EmployeeMainResponse{Vacancy: &a}
			err := response.Send(res)

			if err != nil {
				log.Fatalf("error while sending greet many times responses: %v", err.Error())
			}

		}
	}
	return nil
}


func (s *Server) CreateVacancyService(ctx context.Context, req *proto.CreateRequest) (*proto.CreateResponse, error) {

	JobTitle := req.GetJobTitle()
	CompanyTitle := req.GetCompanyTitle()
	City := req.GetCity()
	Salary := req.GetSalary()
	Description := req.GetDescription()
	EmployeeId := req.GetEmployeeId()

	db, err := sql.Open("mysql", "root:2131@tcp(127.0.0.1:3306)/final_second")
	if err != nil {
		panic(err)
	}

	defer db.Close()

	id, _ := strconv.ParseInt(EmployeeId, 10, 62)



	resSql, err := db.Query(fmt.Sprintf("insert into `vacancy`(Job_title, Company_title, Jober_city, Job_salary, Job_description, user_id) values('%s', '%s', '%s', '%s', '%s', '%d')", JobTitle, CompanyTitle, City, Salary, Description, id))
	if err != nil {
		log.Fatal(err)
	}

	defer resSql.Close()


	res := &proto.CreateResponse{
		Message: "Success",
	}

	return res, nil
}

func(s *Server) OwnVacancy(req *proto.OwnRequest, response proto.OwnVacancyService_OwnVacancyServer) error{

	db, err := sql.Open("mysql", "root:2131@tcp(127.0.0.1:3306)/final_second")
	if err != nil {
		panic(err)
	}

	defer db.Close()

	id, _ := strconv.ParseInt(req.GetEmployeeId(), 10, 62)

	resSql, err := db.Query(fmt.Sprintf("select * from `vacancy` where `user_id` = '%d' order by `Id` DESC", id))
	if err != nil {
		panic(err)
	}

	for resSql.Next() {
		var vacancy Vacancy
		resSql.Scan(&vacancy.Id, &vacancy.JobTitle, &vacancy.CompanyTitle, &vacancy.City, &vacancy.Salary, &vacancy.JobDescription, &vacancy.UserId)


		res := &proto.OwnResponse{VacancyId: vacancy.Id, JobTitle: vacancy.JobTitle, CompanyTitle: vacancy.CompanyTitle, City: vacancy.City, Salary: vacancy.Salary, Description: vacancy.JobDescription}
		err := response.Send(res)

		if err != nil {
			log.Fatalf("error while sending greet many times responses: %v", err.Error())
		}
	}

	return nil

}

func(s *Server) DeleteVacancy(ctx context.Context, req *proto.DeleteRequest) (*proto.DeleteResponse, error) {

	db, err := sql.Open("mysql", "root:2131@tcp(127.0.0.1:3306)/final_second")
	if err != nil {
		panic(err)
	}

	defer db.Close()

	id, _ := strconv.ParseInt(req.GetVacancyId(), 10, 62)



	resSql1, err := db.Query(fmt.Sprintf("delete `responses` from `responses` where `Vacancy_id` = '%d'", id))
	if err != nil {
		log.Fatal(err)
	}

	defer resSql1.Close()

	resSql, err := db.Query(fmt.Sprintf("delete `vacancy` from `vacancy` where `Id` = '%d'", id))
	if err != nil {
		log.Fatal(err)
	}

	defer resSql.Close()


	res := &proto.DeleteResponse{
		Message: "Success",
	}

	return res, nil
}

func(s *Server) ResponsesList(req *proto.ResponsesRequest, response proto.ResponsesListService_ResponsesListServer) error {

	db, err := sql.Open("mysql", "root:2131@tcp(127.0.0.1:3306)/final_second")
	if err != nil {
		panic(err)
	}

	defer db.Close()

	id, _ := strconv.ParseInt(req.GetEmployeeId(), 10, 62)

	resSql, err := db.Query(fmt.Sprintf("select `Job_title`, `Company_title`, `Resume_id`, responses.Id from responses inner join vacancy on responses.Vacancy_id = vacancy.Id where vacancy.user_id = '%d'", id))
	if err != nil {
		log.Fatal(err)
	}

	for resSql.Next() {
		var responses Responses
		resSql.Scan(&responses.JobTitle, &responses.CompanyTitle, &responses.ResumeId, &responses.ResponseId)



		res := &proto.ResponsesResponse{JobTitle: responses.JobTitle, CompanyTitle: responses.CompanyTitle, ResumeId: responses.ResumeId, ResponseId: responses.ResponseId}
		err := response.Send(res)

		if err != nil {
			log.Fatalf("error while sending greet many times responses: %v", err.Error())
		}
	}

	return nil
}


func(s *Server) DeleteResponse(ctx context.Context, req *proto.DeleteResponseRequest) (*proto.DeleteResponseResponse, error) {

	db, err := sql.Open("mysql", "root:2131@tcp(127.0.0.1:3306)/final_second")
	if err != nil {
		panic(err)
	}

	defer db.Close()

	id, _ := strconv.ParseInt(req.GetResponseId(), 10, 62)



	resSql1, err := db.Query(fmt.Sprintf("delete `responses` from `responses` where `Id` = '%d'", id))
	if err != nil {
		log.Fatal(err)
	}

	defer resSql1.Close()


	res := &proto.DeleteResponseResponse{
		Message: "Success",
	}

	return res, nil
}

func main() {
	l, err := net.Listen("tcp", "0.0.0.0:50052")
	if err != nil {
		log.Fatalf("Failed to listen:%v", err)
	}
	s := grpc.NewServer()
	srv := &Server{}
	proto.RegisterEmployeeMainServiceServer(s, srv)
	proto.RegisterCreateVacancyServiceServer(s, srv)
	proto.RegisterOwnVacancyServiceServer(s, srv)
	proto.RegisterDeleteVacancyServiceServer(s, srv)
	proto.RegisterResponsesListServiceServer(s, srv)
	proto.RegisterDeleteResponseServiceServer(s, srv)
	log.Println("Server is running on port:50052")
	if err := s.Serve(l); err != nil {
		log.Fatalf("failed to serve:%v", err)
	}
}